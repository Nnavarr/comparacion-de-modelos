## Evaluación y comparación de modelos de aprendizaje automático

#### Descripción
En este repositorio se encuentran el código y las imágenes utilizadas en los algoritmos empleados en la tesis de evaluación y comparación de modelos de aprendizaje automático.

#### Estructura 
Cada carpeta, a excepción de la carpeta Hopfield, contiene los algoritmos en formato .ipynb (cuadernos Jupyter con código Python ) y unos archivos .pickle que contienen los datos de las imágenes.

La carpeta Hopfield incluye su propio README con las instrucciones necesarias para ejecutar el programa en python.

#### Uso

Los archivos .ipynb pueden ejecutarse utilizando cuadernos de Jupyter o se pueden observar, tanto el código como los experimentos, directamente con GitLab

#### Imágenes
Una gran parte de las imágenes pertenecen al dueño del repositorio y las demás son sacadas de la página https://www.kaggle.com/grassknoted/asl-alphabet
