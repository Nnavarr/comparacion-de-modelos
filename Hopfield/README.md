Codigo tomado y modificado de: https://github.com/takyamamoto/Hopfield-Network

# Hopfield Network

## Requirement
- Python >= 3.5
- numpy
- matplotlib
- skimage
- tqdm
- keras 
- pickle

## Usage
correr `train.py` 


## Reference
- Amari, "Neural theory of association and concept-formation", SI. Biol. Cybernetics (1977) 26: 175. https://doi.org/10.1007/BF00365229
- J. J. Hopfield, "Neural networks and physical systems with emergent collective computational abilities", Proceedings of the National Academy of Sciences of the USA, vol. 79 no. 8 pp. 2554–2558, April 1982.
